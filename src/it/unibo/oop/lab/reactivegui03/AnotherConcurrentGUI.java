package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnotherConcurrentGUI extends JFrame{
	private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
	private final JLabel display = new JLabel();
	private final JButton up = new JButton("up");
	private final JButton down = new JButton("down");
	private final JButton stop = new JButton("stop");
	final Agent agent = new Agent();
	final Agent_stop agent_stop = new Agent_stop();
	
	public AnotherConcurrentGUI() {
		final Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int) (screensize.getWidth() * WIDTH_PERC), (int) (screensize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		final JFrame frame = new JFrame();
		final JPanel panel = new JPanel();
		panel.add(display);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		this.getContentPane().add(panel);
		this.setVisible(true);
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(agent);
		final ExecutorService executor2 = Executors.newSingleThreadExecutor();
		executor2.submit(agent_stop);
		
        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
            }
        });
        
        up.addActionListener(new ActionListener () {
        	
        	public void actionPerformed(final ActionEvent e) {
        		agent.up();
        	}
        });
        
        down.addActionListener(new ActionListener () {
        	
        	public void actionPerformed(final ActionEvent e) {
        		agent.down();
        	}
        });
	}
        
        private class Agent implements Runnable {
        	
        	private volatile boolean up;
        	private volatile boolean down;
        	private volatile boolean stop;
            private int counter;
             
            public void run() {
                while (!this.stop) {
                    try {
                        /*
                         * All the operations on the GUI must be performed by the
                         * Event-Dispatch Thread (EDT)!
                         */
                    	SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                            	AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                            }
                        });
                    	
                    	if (!this.stop) {
                    		if(this.up) {
                    			this.counter++;
                    		}	
                    		else if (this.down) {
                    			this.counter--;                   			
                    		}
                    		Thread.sleep(100);
                    	}
                        
                    } catch (InvocationTargetException | InterruptedException ex) {
                        /*
                         * This is just a stack trace print, in a real program there
                         * should be some logging and decent error reporting
                         */
                        ex.printStackTrace();
                    }
                }
            }
            
            public void stopCounting() {
                this.stop = true;
            }
            
            public void up() {
            	if(!this.stop) {
            		this.up = true;
            		this.down = false;
            	}
            	else if (this.stop) {
            		this.up = false;
            	}
            }
            
            public void down() {
            	if(!this.stop) {
            		this.down = true;
            		this.up = false;
            	}
            	else if (this.stop) {
            		this.down = false;
            	}
            }
        }

        private class Agent_stop implements Runnable {
            
            public void run() {
            	try {
            		Thread.sleep(3000);
            		AnotherConcurrentGUI.this.agent.stopCounting();
            		
            		SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                        	AnotherConcurrentGUI.this.up.setEnabled(false);
                        	AnotherConcurrentGUI.this.down.setEnabled(false);
                        	AnotherConcurrentGUI.this.stop.setEnabled(false);
                        }
                    });
            	} catch (InvocationTargetException | InterruptedException ex) {
                /*
                 * This is just a stack trace print, in a real program there
                 * should be some logging and decent error reporting
                 */
                ex.printStackTrace();
            	}
            }
        }
}
