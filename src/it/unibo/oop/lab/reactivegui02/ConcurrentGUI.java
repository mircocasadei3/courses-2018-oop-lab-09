package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.*;

public final class ConcurrentGUI extends JFrame{

	private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
	private final JLabel display = new JLabel();
	private final JButton up = new JButton("up");
	private final JButton down = new JButton("down");
	private final JButton stop = new JButton("stop");
	
	public ConcurrentGUI() {
		final Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int) (screensize.getWidth() * WIDTH_PERC), (int) (screensize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		final JFrame frame = new JFrame();
		final JPanel panel = new JPanel();
		panel.add(display);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		this.getContentPane().add(panel);
		this.setVisible(true);
		final Agent agent = new Agent();
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(agent);
        
        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
            }
        });
        
        up.addActionListener(new ActionListener () {
        	
        	public void actionPerformed(final ActionEvent e) {
        		agent.up();
        	}
        });
        
        down.addActionListener(new ActionListener () {
        	
        	public void actionPerformed(final ActionEvent e) {
        		agent.down();
        	}
        });
	}
        
        private class Agent implements Runnable {
        	
        	private volatile boolean up = false;
        	private volatile boolean down = false;
        	private volatile boolean stop;
            private int counter;
            
            
            
            public void run() {
                while (true) {
                    try {
                        /*
                         * All the operations on the GUI must be performed by the
                         * Event-Dispatch Thread (EDT)!
                         */
                    	SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                ConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                            }
                        });
                    	
                    	if (!this.stop) {
                    		if(this.up) {
                    			this.counter++;
                    		}	
                    		else if (this.down) {
                    			this.counter--;                   			
                    		}
                    		Thread.sleep(100);
                    	}
                        
                    } catch (InvocationTargetException | InterruptedException ex) {
                        /*
                         * This is just a stack trace print, in a real program there
                         * should be some logging and decent error reporting
                         */
                        ex.printStackTrace();
                    }
                }
            }
            public void stopCounting() {
                this.stop = true;
            }
            
            public void up() {
            	if(!this.stop) {
            		this.up = true;
            		this.down = false;
            	}
            	else if (this.stop) {
            		this.up = false;
            	}
            }
            
            public void down() {
            	if(!this.stop) {
            		this.down = true;
            		this.up = false;
            	}
            	else if (this.stop) {
            		this.down = false;
            	}
            }
        }
}